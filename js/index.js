/*---Теоретичні питання---
1) Опишіть, як можна створити новий HTML тег на сторінці.
Необхідно визначитися з назвою тегу та його призначенням (далі використати у HTML).
За допомогою JS та CSS визначити поведінку тегу.
2) Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр функції визначає позицію, куди слід вставити HTML-вміст щодо елемента,для якого викликається метод.
Існує 4 варіанти параметра: "beforebegin", "afterbegin", "beforeend", "afterend".
3) Як можна видалити елемент зі сторінки?
Для початку вибираємо елемент за допомогою методу getElementById() або querySelector(), а потім викликати метод remove()
для вибраного елемента. Також можна видалити елемент з батьківського елемента, за допомогою властивості parentNode і 
методу removeChild().

---Практичне завдання---*/
const cities = ["New York", "London", "Tokio", "Paris"];
displayArrayAsList(cities);
const numbers = ["4", "8", "15", "16", "23"];
displayArrayAsList(numbers);

const container = document.getElementById("my-list-container");

function displayArrayAsList(array, parent = document.body) {
    const listElement = document.createElement('ul');

    for(let i = 0; i < array.length; i++) {
        const listItemElement = document.createElement('li');
        listItemElement.textContent = array[i];
        listElement.appendChild(listItemElement);
    };
    parent.appendChild(listElement);
}


 